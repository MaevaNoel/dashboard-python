# Table des matières

[<ins>**I. RAPPORT D&#39;ANALYSE**</ins>](#rapport-danalyse)

[I.1 Introduction](#introduction)

[I.2 Mode opératoire](#mode_operatoire)

[I.3 Difficultés rencontrées](#difficultés-rencontrées)

[I.4 Ressources utilisées](#ressources-utilisées)

[<ins>**II. USER GUIDE**</ins>](#user-guide)

[II.1 Packages nécessaires](#packages-nécessaires)

[II.2 Lancer l&#39;application](#lancer-lapplication)

[*II.2.1 Sous Windows*](#sous-windows)

[*II.2.2 Sous Linux*](#sous-linux)

[II.3 Structure des pages](#structure-des-pages)

[<ins>**III. DEVELOPER GUIDE**</ins>](#developer-guide)

[III.1 Technologies](#technologies)

[III.2 Environnement de développement](#Environnement-de-développement)

[*III.2.1 Sous Windows*](#sous-windows-1)

[*III.2.2 Sous Linux*](#sous-linux-1)

[III.3 Structure du code](#structure-du-code)

# RAPPORT D&#39;ANALYSE

## Introduction

Au fil des années, la crise climatique empire. Que ce soit la déforestation, le CO2 généré par les voitures, les exploitations pétrolières ou encore la décharge de déchets, toutes ces causes ont comme facteur commun l&#39;Homme. Cela a pour conséquence de faire augmenter la température moyenne de la planète, ce qui résulte par la perturbation d&#39;équilibres écologique, comme par exemple la fonte des glaciers ou la disparition de certaines espèces. Afin de sensibiliser à ce sujet, nous avons décider de réaliser un dashboard montrant l&#39;évolution de certains facteurs de cette crise au fil du temps dans le monde. Nous espérons vous aider à comprendre l&#39;évolution de cette crise climatique.

## Mode opératoire
Pour ce projet, nous nous sommes rencontrées une à deux fois par semaine. Durant ces échanges, nous décidions sur quoi travailler dans la semaine et mettions en commun ce que nous avions chacune fait.

## Difficultés rencontrées
Au cours de ce projet, la principale difficulté que nous avons rencontré a été de trouver un set de données assez complet pour nous permettre de proposer de nombreux graphiques et illustrations. Ensuite,
il nous a fallu trier toutes les données et sélectionner les plus pertinentes afin d'avoir un dashboard complet, lisible et pertinent.
Une autre difficulté a été d'exploiter les données selon les années. En effet, notre source de données contient une colonne par année et il n'a pas été évident au départ de trier et afficher nos données selon ces colonnes, d'autant que certaines
ne contiennent pas de donnée. Cela nous a conduit a géré le cas de l'absence de données, afin de ne pas avoir d'erreurs.

## Ressources utilisées
Le fichier de données utilisé provient de ce site : [https://www.gapminder.org/data/](https://www.gapminder.org/data/)

# USER GUIDE

## Packages nécessaires

Il est essentiel d'avoir installer Anaconda au préalable pour un OS Windows, le téléchargement se fait via ce site : [https://www.anaconda.com/products/individual/](https://www.anaconda.com/products/individual/) .

Avant de commencer les installations des packages, il faut lancer la commande : **conda update --all**

Cette commande permet de mettre à jour les packages conda et évite l&#39;apparition de certaines erreurs lors de l&#39;installation de certains packages.

| **Nom du package** | **Commande d&#39;installation** |
| --- | --- |
| **Sous Windows** |
| **Plotly express** | pip install plotly --upgrade |
| **Pandas** | conda install --channel conda-forge pandas |
| **Geopandas** | conda install --channel conda-forge geopandas |
| **Dash** | conda install --channel conda-forge dash |
| **Dash\_bootstrap\_components** | conda install -c conda-forge dash-bootstrap-components |
| **Sous Linux** |
| **Plotly express** | python3 -m pip install plotly-express |
| **Pandas** | python3 -m pip install pandas |
| **Geopandas** | python3 -m pip install geopandas |
| **Dash** | python3 -m pip install dash |
| **Dash\_bootstrap\_components** | python3 -m pip install dash-bootstrap-components |

## Lancer l&#39;application

### Sous Windows

Tout d&#39;abord, il faut installer Python. Pour cela, téléchargez l&#39;installeur Windows disponible sur ce site : [https://www.python.org/downloads/release/python-387/](https://www.python.org/downloads/release/python-387/) .

Une fois Python installé, on peut lancer l&#39;application, ouvrez alors un terminal. Il suffit de chercher « Invite de commandes » dans la barre de recherche Windows. Il est aussi possible d&#39;appuyer sur les touches Windows et R, de taper « cmd » puis appuyez sur OK.

Il faut ensuite se positionner dans le dossier où se trouve les fichiers sources. Pour cela, utilisez la commande « cd _cheminVersLeDossier_ ». Si le dossier se trouve sur un autre disque (le disque est indiqué par la première lettre du chemin, il s&#39;agit habituellement du disque C), il faut changer de disque en faisant la commande « _lettre_: ».

Une fois dans le bon dossier, faites la commande « dir », vous devriez voir apparaitre des noms de fichiers et de dossiers, dont **main.py**.

Si c&#39;est le cas, vous pouvez écrire la commande « python main.py ». Si au contraire, le nom du fichier n&#39;apparait pas cela signifie que vous n&#39;êtes pas dans le bon dossier.

Pour accéder au dashboard, il faut ouvrir une page internet et taper l&#39;adresse qui s&#39;est affichée dans le terminal.

_Exemple :_

![alt text](readMeImages/exemple.jpg)

Ici, l&#39;adresse à afficher est 127.0.0.1:8085

### Sous Linux

Tout d&#39;abord, il faut que Python soit installé sur votre poste. Vous pouvez vérifier que Python est bien installé avec la commande « python3 ». Sinon, ouvrez un terminal. 

Il suffit de chercher « Terminal » dans les applications, puis exécutez la commande « apt-get install python3 » .

Une fois Python installé, on peut lancer l&#39;application, toujours dans le terminal, il faut se positionner dans le dossier où se trouve les fichiers sources. Pour cela, 

utilisez la commande « cd _cheminVersLeDossier_ ».

Une fois dans le bon dossier, faites la commande « dir », vous devriez voir apparaitre des noms de fichiers et de dossiers, dont **main.py**.

Si c&#39;est le cas, vous pouvez écrire la commande « python3 main.py ». Si au contraire, le nom du fichier n&#39;apparait pas cela signifie que vous n&#39;êtes pas dans le bon dossier.

Pour accéder au dashboard, il faut ouvrir une page internet et taper l&#39;adresse qui s&#39;est affichée dans le terminal.

_Exemple :_

![alt text](readMeImages/exempleLinux.jpg)

Ici, l&#39;adresse à afficher est 127.0.0.1:8050

## Structure des pages

L&#39;application est divisée en deux pages principales.

Sur la première page, il est possible de voir deux cartes du monde. Il est possible de choisir l&#39;indicateur à afficher sur les cartes grâce à la liste déroulante.

![alt text](readMeImages/ViewPage1.png)

Il est possible de survoler la carte avec la souris pour obtenir plus d&#39;informations.

En cliquant sur un pays, que ce soit sur la première ou deuxième carte, une nouvelle page s&#39;affiche. Cette page contient plus d&#39;informations concernant les facteurs de la crise climatique. Cliquons par exemple sur la France. Cette page s&#39;affiche alors :

![alt text](readMeImages/ViewPage2.png)

Il est possible de survoler les différents graphiques avec la souris afin d&#39;avoir plus d&#39;informations.

# DEVELOPER GUIDE

## Technologies

Cette application peut être modifiée par tout développeur de tout niveau. Si vous souhaitez plus d&#39;informations sur les technologies utilisées :

| **Technologie** | **Lien** |
| --- | --- |
| **Anaconda** | [https://docs.anaconda.com/anaconda/user-guide/getting-started/](https://docs.anaconda.com/anaconda/user-guide/getting-started/) |
| **Python** | [https://docs.python.org/3/](https://docs.python.org/3/) |
| **Plotly express** | [https://plotly.com/python/plotly-express/](https://plotly.com/python/plotly-express/) |
| **Dash** | [https://dash.plotly.com/](https://dash.plotly.com/) |
| **Pandas** | [https://pandas.pydata.org/](https://pandas.pydata.org/) |
| **Geopandas** | [https://geopandas.org/](https://geopandas.org/) |

## Environnement de développement

### Sous Windows

Afin de modifier le code, il faut que Python soit installé sur votre poste. Nous allons vous montrer ici comment installer Anaconda ainsi que Visual Studio Code, ce qui vous permettra de coder en Python.

Pour télécharger Visual Studio Code, allez sur cette page : [https://code.visualstudio.com/download](https://code.visualstudio.com/download) . Vous pourrez ensuite lancer l&#39;installation en cliquant sur le fichier téléchargé.

Pour Anaconda, commencez par télécharger le programme sur cette page : [https://www.anaconda.com/products/individual](https://www.anaconda.com/products/individual) . Vous pourrez ensuite lancer l&#39;installateur.

Une fois Anaconda installé, vous avez deux choix. Vous pouvez rechercher « Anaconda Prompt » dans la barre de recherche Windows. Placez vous ensuite dans le dossier où se trouve les codes sources grâce à la commande « cd _lien_ ». Une fois dans le bon répertoire, vous pouvez lancer la commande « code . »

Le second choix est de rechercher « Anaconda Navigator » dans la barre de recherche Windows. Vous pourrez ensuite choisir votre éditeur pour modifier le code.

Il vous faut ensuite installer les packages. Pour cela, allez à la partie Packages nécessaires.

### Sous Linux

Afin de modifier le code, il faut que Python soit installé sur votre poste. Vous pouvez vérifier que Python est bien installé avec la commande « python3 ». Sinon, exécutez la commande « apt-get install python3 ».

Nous allons vous montrer ici comment installer pip ainsi que Visual Studio Code, ce qui vous permettra de coder en Python.

Pour télécharger Visual Studio Code, allez sur cette page : [https://code.visualstudio.com/download](https://code.visualstudio.com/download) . Vous pourrez ensuite lancer l&#39;installation en cliquant sur le fichier téléchargé.

Pour installer pip, exécutez la commande « apt-get install python3-pip » .

Il vous faudra ensuite installer les packages. Pour cela, allez à la partie Packages nécessaires.

## Structure du code

Le code est séparé en deux fichiers :

- main.py : Ce fichier contient la fonction main. Celle-ci permet le lancement du dashboard. Elle contient donc l&#39;affichage du dash ainsi que la gestion du callback.
- functions.py : Ce fichier contient les fonctions permettant de récupérer les données à afficher et la création des différentes figures à afficher.

![alt text](readMeImages/schema_fonctions.jpg)

### Ajouter des figures

Pour ajouter des figures, vous pouvez les ajouter dans la fonction _figureCreation_.

Il faudra ensuite appeler la fonction _createGraph_ dans le fichier main.py afin de pouvoir afficher le graphique.

Si vous souhaitez ajouter un indicateur pour un pays, il faut penser à ajouter cette nouvelle figure dans le tableau des outputs du call back et dans le retour de la fonction _update\_figure_.

Si vous souhaitez ajouter une carte du monde et qu&#39;elle affiche les données de chaque pays, il faut l&#39;ajouter aux inputs du callback et dans le retour de la fonction _update\_figure_.