#
# Imports
#
import plotly.express as px
import pandas as pd
import geopandas as gpd
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import math


#
# Data
#
import functions as f

#
# Main
#
if __name__ == '__main__': 
    app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP]) 

    # Dashboard html display
    app.layout = html.Div(        
                        children=[
                            # Creation of the header by calling associated function
                            f.createHeader(),

                            # Creation of an alert on the slider
                            html.Div(
                                [
                                    dbc.Alert(
                                        [
                                            "You can go through the years with the slider."
                                        ],
                                        color="primary",
                                        id="alertInformations"
                                    )
                                ]
                            ),

                            # Element explaining how to use the dashboard
                            html.Div(children='Click on a country to display its data or select an indicator to display on the map.',
                                        id='description', className='description'),
                            
                            # Creation of the homepage
                            html.Div(id="HomePage", className="homePageShowing", children=
                                [
                                    # Dropdown element allowing user to select indicators
                                    dcc.Dropdown(
                                        id="ddl",
                                        className='ddl',
                                        options=[
                                            {'label': 'Population', 'value': "pop"},
                                            {'label': 'CO2 emissions', 'value': "co2"},
                                            {'label': 'Electricity production from nuclear sources', 'value': "nuclearElectricity"},
                                            {'label': 'Forest area (in %)', 'value': "forest"}
                                        ],
                                        value="pop",
                                        clearable=False
                                    ),
                                    # Slider element allowing user to see world data through the years
                                    html.Div(id="sliderElements", className="slider",
                                        children=[f.createSlider(),
                                        html.Button("Play", id="buttonPlay", n_clicks=0, className="buttons"),
                                        html.Button("Stop", id="buttonStop", n_clicks=0, className="buttons")]
                                    ),
                                    
                                    # Two maps showing world data 
                                    html.Div(id="maps", children=
                                        [
                                            f.createGraph('graphWorldMap', 'figWorld', "worldMaps"),   
                                            f.createGraph('graphWorldMapScatter', 'figWorldScatter', "worldMaps"),
                                        ]
                                    ),

                                    # Interval for the animated slider 
                                    dcc.Interval(id='interval',
                                        interval=3*1000, # in milliseconds
                                        n_intervals=0,
                                        disabled=True
                                    )
                                ]
                            ),                 

                            # Creation of the country data page (after click on a country displayed on the map)                
                            html.Div(id="DetailsPage", className="detailsPageNotShowing", children=
                                [
                                    f.createGraph('graphCO2', 'figCO2', "graphs"),
                                    f.createGraph('graphNuclearElec', 'figNuclearElectricity', "graphs"),
                                    f.createGraph('graphForestArea', 'figForestArea', "graphs"),                            
                                    f.createGraph('graphPopulation', 'figPopulation', "graphs"),
                                ]
                            ),
                            
                            # Creation of the footer by calling associated function
                            f.createFooter()
                    ]
    )

    # App callback function to handle user input 
    @app.callback(
        # List of all outputs we wish to modify on event
        [
            Output(component_id='graphWorldMap', component_property='figure'),
            Output(component_id='graphWorldMapScatter', component_property='figure'),

            Output(component_id='graphForestArea', component_property='figure'),
            Output(component_id='graphCO2', component_property='figure'),
            Output(component_id='graphNuclearElec', component_property='figure'),
            Output(component_id='graphPopulation', component_property='figure'),
            
            Output(component_id='description', component_property='children'),
            Output(component_id='alertInformations', component_property='children'), 
            Output(component_id="HomePage", component_property="className"),            
            Output(component_id="DetailsPage", component_property="className")
      
        ],
        # List of all input properties we observe to update on event 
        [
            Input(component_id='graphWorldMap', component_property='clickData'),
            Input(component_id='graphWorldMapScatter', component_property='clickData'),
            Input(component_id='ddl', component_property='value'),
            Input(component_id='year-slider', component_property='value')
        ]
    )

    def update_figure(click_data_wm, click_data_wms, value, year_value):
        '''
        This function is used to change the display according to the user's actions (click event, return to home, etc.)

        click_data_wm: name of the country clicked on the first map
        click_data_wms: name of the country clicked on the map scatter
        value: value of the drop down list, contains name of what to display on the world map 
        year_value: value of the year to be displayed
        '''
        # Check if any country has been clicked
        if click_data_wm is not None :
            countryClicked = click_data_wm
        elif click_data_wms is not None :
            countryClicked = click_data_wms
        else:
            countryClicked = None
            
        
        if countryClicked is not None : # Means that user has clicked on a country
            # Update our figures
            f.figures = f.figureCreation(countryClicked['points'][0]['hovertext'], str(year_value))

            # Change our message text
            message = html.A("Click here to display the world map", href="./main.py", className="alert-link")
            
            # Change our page description content
            descriptionContent = "You are currently consulting the " + countryClicked['points'][0]['hovertext'] + " data."

            # Change our style class name 
            classNameHomePage = "homePageNotShowing"
            classNameDetailsPage = "detailsPageShowing"

            # Return the different page elements
            return [
                f.figures["figWorld"], 
                f.figures["figWorldScatter"],  
                f.figures["figForestArea"],  
                f.figures["figCO2"], 
                f.figures["figNuclearElectricity"], 
                f.figures["figPopulation"], 
                descriptionContent, message, classNameHomePage, classNameDetailsPage]
           
        else: # Means that the world map is being displayed
            # Update our figures
            fig = f.figureCreation("France", str(year_value))

            # Update our world map
            fig["figWorld"] = f.getWorldMap(value, str(year_value))[0]
            fig["figWorldScatter"] = f.getWorldMap(value, str(year_value))[1]

            # Update our message text
            message = f.getWorldMap(value, str(year_value))[2]

            # Change our page description's content
            descriptionContent = "Click on a country to display its data or select an indicator to display on the map."

            # Update style class name of graphs parts
            classNameHomePage = "homePageShowing"
            classNameDetailsPage = "detailsPageNotShowing"

            # Return the different page elements
            return [
                fig["figWorld"], fig["figWorldScatter"],
                f.figures["figForestArea"], 
                f.figures["figCO2"],
                f.figures["figNuclearElectricity"],
                f.figures["figPopulation"],
                descriptionContent, message, classNameHomePage, classNameDetailsPage]
            

    # App callback function to handle user input on the slider's buttons
    @app.callback(Output('interval', 'disabled'), # Output we want to modify on button click
                 # Inputs we observe for click events
                [Input('buttonPlay', 'n_clicks'), 
                Input('buttonStop', 'n_clicks')])
                
    def updatePlayStopSlider(nbClickPlay, nbClickStop):
        '''
        This function is used to play or pause the slider's animation

        nbClickPlay: number of clicks on play button
        nbClickStop: number of clicks on stop button
        '''
        if nbClickPlay > nbClickStop: 
            return False # Play
        return True # Stop

    # App callback function to handle user input on the slider
    @app.callback(Output('year-slider', 'value'), # Output we want to modify on animation
                [Input('interval', 'n_intervals')]) # Input we observe to change slider's value

    def on_tick(n_intervals):
        '''
        This function is used to animate the slider and change its value every 3 seconds

        n_intervals: interval used to animate the world data display 
        '''
        if n_intervals is None: return 0
        years = f.getYears()
        if n_intervals == 0: return years[0]
        return years[(n_intervals*5)%len(years)]
        
    #
    # RUN APP
    #

    app.run_server(debug=True)