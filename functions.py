#
# Imports
#
import plotly.express as px
import pandas as pd
import geopandas as gpd
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import math

#
# Data
#

# Create a variable with our datafile
try:
    climatFile = pd.read_csv('datasets/climat.csv')
except FileNotFoundError:
    print("File not found")
    exit()

try: 
    # Rename column so we can use it later
    climatFile.rename(columns={"Indicator Code": "IC", "Country Code" : "CC", "Country Name" : "CN"},
                        inplace = True)  
except KeyError:
    print("Column name not found")
    exit()

###
### Functions
###
def getYears():
    '''
    This function returns all years available in file
    '''
    return [ int(name) for name in climatFile.columns if name.isnumeric() ]

# We get all years to use them in the other functions
years = getYears()

def getDataForWorldMap(indicator, name, year_value):
    '''
    This function is used to update data to display on the world map

    indicator: it's the indicator code for what we want to display
    name: name of what we want to display
    year_value: selected year
    
    returns: dataframe containing geopandas data to display world map and data we want to fill the map
    '''

    # Get data thanks to query with indicator for parameter and replace NaN with 0
    query = climatFile.query("IC == '"+indicator+"'").fillna(0)

    # Get array with country and data wanted
    frame = {name : query[year_value], "CountryCode" : query['CC']}
    
    # Transform to dataframe
    result = pd.DataFrame(frame)

    # Get dataframe from gapminder 
    dfGapminder = px.data.gapminder().query("year==2007")

    # Join dataframes (gapminder and indicator data/countries)
    dfJoined = dfGapminder.set_index("iso_alpha").join(result.set_index("CountryCode")).fillna(0)

    return dfJoined

def getFigureByCountry(indicator, countryName):
    '''
    This function is used to get data to display on graphs 

    indicator: indicator of the data we want
    countryName: name of the country we want to see data of

    returns: array containing data for each year
    '''

    # Get data for a specific country
    query = climatFile.query("IC =='"+indicator+"' & CN == '" + countryName + "'").fillna(0)

    # Initialize array with value of each year
    return [ round(query[name].sum()) for name in climatFile.columns if name.isnumeric() ]

def figureCreation(country, year_value):
    '''
    This function is used to create the different figures and populate them with the right data

    country: if a country has been clicked we get only this country's data
    year_value: get data according to selected year

    returns: dictionary containing all figures
    '''

    # Get the data for the world map
    worldMapData = getDataForWorldMap("SP.POP.TOTL", 'Population', year_value).fillna(0)

    # Creation of the choropleth map
    figWorld = px.choropleth(worldMapData, locations=worldMapData.index, 
                    color="Population",
                    hover_name="country",
                    color_continuous_scale=px.colors.sequential.YlOrRd,
                    title="World map",                    
                    range_color=(0,50000000),
                    )

    # Creation of the scatter map
    figWorldScatter = px.scatter_geo(worldMapData, locations=worldMapData.index,color="Population",hover_name="country",
                                    size=worldMapData.Population
                                    )
                                

    # Creation of the 4 figures displayed on country click
    figPopulation = createHistogram(years, getFigureByCountry("SP.POP.TOTL", country), "Total of the population through the years", "Years", "Population (in millions)")
    figCO2 = createLineFigure(years, getFigureByCountry("EN.ATM.CO2E.KT", country), "Air CO2 rates through the years", "Years", "CO2 rates in kt (kilotons)")
    figNuclearElectricity = createLineFigure(years, getFigureByCountry("EN.ATM.CO2E.KT", country), "Nuclear electricity production through the years (% of total)", "Years", "% of electricity that comes from a nuclear source")
    figForestArea = createLineFigure(years, getFigureByCountry("AG.LND.FRST.ZS", country), "Size of forest areas through the years (% of land area)", "Years", "% of land area")
    
    # Dictionary containing all created figures
    figureDict = {"figPopulation" : figPopulation,
    "figCO2" : figCO2,
    "figNuclearElectricity" : figNuclearElectricity,
    "figForestArea" : figForestArea,
    "figWorld" : figWorld,
    "figWorldScatter": figWorldScatter
    }

    return figureDict

def createLineFigure(p_x, p_y, p_title, x_title, y_title):
    '''
    Create figure with plotly express

    p_x: data to display in x axe
    p_y: data to display in y axe
    p_title: title to display for the figure
    x_title: title of x legend
    y_title: title of y legend

    returns: figure created
    '''
    return px.line(climatFile, x=p_x, y=p_y, title=p_title, labels={'x':x_title, 'y':y_title},
                template="simple_white", color_discrete_sequence=px.colors.qualitative.Pastel2)

def createHistogram(p_x, p_y, p_title, x_title, y_title):
    '''
    Create histogram with plotly express

    p_x: data to display in x axe
    p_y: data to display in y axe
    p_title: title to display for the figure
    x_title: title of x legend
    y_title: title of y legend

    returns: figure created
    '''
    return px.histogram(climatFile, x=p_x, y=p_y, title=p_title, labels={'x':x_title, 'y':y_title},
                template="simple_white", color_discrete_sequence=px.colors.qualitative.Pastel2,
                nbins=60, facet_col_spacing=1)

# Initialization of figures 
figures = figureCreation("France", "2020")

def createGraph(p_id, p_figureName, p_className):
    '''
    Create a card containing a graph in function of parameters

    p_id: id of the graph
    p_figureName: figure name of the graph
    p_className: class name to display for the graph

    returns: card created
    '''
    return dbc.Card(className=p_className, children=
        [
            dbc.CardBody(
                [
                    html.P(
                        dcc.Graph(
                            id=p_id,
                            figure=figures[p_figureName]
                        ),
                        className="card-text",
                    )
                ]
            )
        ],
        id="card_" + p_id
    )

def createHeader():
    '''
    Create our header for the page

    returns: header created
    '''
    return html.Div(
                dbc.Jumbotron(id="header", children=
                [
                    dbc.Container(
                        [
                            # Title of the dashboard
                            html.H1("Dashboard on climate change", className="display-3"),
                            # Introduction of the dashboard
                            html.P(
                                "Climate change represents one of the most important concern of our century. "
                                "Many factors must be monitored and we still have a lot to do if we "
                                "want to make sure our planet survives. Today, a lot of people fail to "
                                "realize the risks at hand, and the gravity of the situation. "
                                "This dashboard allows you to witness the evolution of different environmental factors "
                                "throughout the years and to consult data by country. ",
                                className="lead",
                            ),
                        ],
                        fluid=True,
                    ),
                ],
                fluid=True
            ))

def createFooter():
    '''
    Create our footer for the page

    returns: footer created
    '''
    return html.Div(
                dbc.Alert("© DESRIAUX Lucie et NOËL Maëva", color="secondary", id="footer")
            )

def createSlider():
    '''
    Create the years slider

    returns: slider created
    '''
    return dcc.Slider(
                id="year-slider",
                min=years[0],
                max=years[len(years) - 1],
                value=years[len(years) - 1],
                marks={str(i): str(i) for i in years if i % 5 == 0}
            )


def getWorldMap(value, year_value):
    '''
    Change value of figure in dictionnary

    value: indicator selected
    year_value: year selected

    returns: call function createWorldMap() 
    '''
    if value == "pop":
        return createWorldMap("SP.POP.TOTL", 'Population', 0, 50000000, year_value)
        
    elif value == "co2":
        return createWorldMap("EN.ATM.CO2E.KT", 'CO2Emissions', 0, 1000000, year_value)

    elif value == "forest":        
        return createWorldMap("AG.LND.FRST.ZS", 'ForestArea', 0, 60, year_value)

    else:
        return createWorldMap("EG.ELC.NUCL.ZS", 'NuclearElectricity', 0, 20, year_value)

def createWorldMap(indicatorCode, indicatorName, min, max, year_value):
    '''
    Create choropleth to display world map

    indicatorCode: code of the indicator
    indicatorName: name of the indicator
    min: min value
    max: max value
    year_value: year selected

    returns: choropleth and scatter maps
    '''
    worldData = getDataForWorldMap(indicatorCode, indicatorName, year_value).fillna(0)

    if(indicatorName == 'Population') :
        s = worldData.Population
    elif(indicatorName == 'CO2Emissions'):
        s = worldData.CO2Emissions
    elif(indicatorName == 'ForestArea'):
        s = worldData.ForestArea
    elif(indicatorName == 'NuclearElectricity'):
        s = worldData.NuclearElectricity

    message = "You can go through the years with the slider."
    if((worldData[indicatorName] == 0).all()) : 
        message += " Sorry, there is no data for year " + year_value + " concerning the " + indicatorName + " indicator." 

    return [
            # Choropleth map
            px.choropleth(
                        worldData, 
                        locations=worldData.index, 
                        color=indicatorName,
                        hover_name="country",
                        color_continuous_scale=px.colors.sequential.YlOrRd,
                        title="World map concerning " + indicatorName + " in " + year_value + " (color indications)",     
                        range_color=(min, max)   
                        ),
            # Scatter map
            px.scatter_geo(
                        worldData, 
                        locations=worldData.index,
                        color=indicatorName,
                        hover_name="country",
                        title="World map concerning " + indicatorName + " in " + year_value + " (scatter and color indications)",  
                        range_color=(min, max),
                        size=s
                        ),
            message

    ]
